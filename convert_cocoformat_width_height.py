import json

# Lire le fichier JSON d'annotations
with open('data/image_dataset_labelled_GroundingDINO/grounding_DINO_classification_DOTA.json', 'r') as f:
    data = json.load(f)

annotations = data['annotations']

def round_decimal(value, decimals=1):
    return round(value, decimals)

def convert_bbox_format(annotations):
    converted_annotations = []
    for annotation in annotations:
        x_min, y_min, x_max, y_max = annotation['bbox']

        # Gérer les valeurs négatives
        x_min = max(x_min, 0)
        y_min = max(y_min, 0)
        width = x_max - x_min
        height = y_max - y_min

        x_min = round_decimal(x_min)
        y_min = round_decimal(y_min)
        width = round_decimal(width)
        height = round_decimal(height)

        converted_bbox = [x_min, y_min, width, height]
        
        converted_annotation = {
            "image_id": annotation["image_id"],
            "category_id": annotation["category_id"],
            "bbox": converted_bbox,
            "score": annotation["score"]
        }
        converted_annotations.append(converted_annotation)
    
    return converted_annotations

converted_annotations = convert_bbox_format(annotations)




# Préparer les données pour le fichier JSON de sortie
output_data = {
    "annotations": converted_annotations
}

# Enregistrer dans un nouveau fichier JSON
with open('converted_annotations_coco_format_wh.json', 'w') as f:
    json.dump(output_data, f, indent=4)
