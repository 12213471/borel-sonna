import cv2
import os
import json
import re
import supervision as sv
from difflib import SequenceMatcher
from groundingdino.util.inference import load_model, load_image, predict, annotate


BOX_TRESHOLD = 0.35
TEXT_TRESHOLD = 0.25
CONFIG_PATH = "groundingdino/config/GroundingDINO_SwinB_cfg.py"
#WEIGHTS_NAME = "groundingdino_swint_ogc.pth"
WEIGHTS_NAME = "groundingdino_swinb_cogcoor.pth"
WEIGHTS_PATH = os.path.join("weights", WEIGHTS_NAME)


print(CONFIG_PATH, "; exist:", os.path.isfile(CONFIG_PATH))
print(WEIGHTS_PATH, "; exist:", os.path.isfile(WEIGHTS_PATH))

TEXT_PROMPT = "planes"
#TEXT_PROMPT = "plane . ship . storage-tank . baseball-diamond . tennis-court . basketball-court . basketball-court . ground-track-field . harbor . bridge . small-vehicle . large-vehicle . roundabout . swimming-pool . helicopter . soccer-ball-field . container-crane"

#
CATEGORIES = TEXT_PROMPT.split(" . ")
print(CATEGORIES)
#IMAGE_NAME = "0000.jpg"
input_folder = "test_Dinh"
#input_folder = "data/image_dataset/dota_dataset/coco/test2017"
#output_folder = "data/image_dataset_labelled_GroundingDINO"
output_folder = "test_Dinh/results"
#json_input_path = "data/image_dataset/dota_dataset/coco/annotations/instances_test2017_modified_data.json"
#json_output_path = "data/image_dataset_labelled_GroundingDINO/grounding_DINO_classification_DOTA.json"
#json_input_path = "data/image_dataset/dota_dataset/coco/annotations/instances_val2017.json"
json_output_path = "test_Dinh/grounding_DINO_classification_DOTA.json"

### MES TESTS

#input_folder = "data/image_dataset_test"
#output_folder ="data/image_dataset_labelled_data_mes_tests"
#json_input_path = "data/image_dataset/dota_dataset/coco/annotations/instances_test2017_modified_data.json"
#json_output_path = "data/image_dataset_labelled_data_mes_tests/grounding_DINO_classification_DOTA.json"

#LES VRAIS TESTS
#input_folder = "data/DOTA_dataset_test_2017"
#output_folder ="data/DOTA_dataset_test_2017_labelled"
json_input_path = "data/image_dataset/dota_dataset/coco/annotations/instances_test2017.json"
#json_output_path = "data/DOTA_dataset_test_2017_labelled/gdino_classification_DOTA_test.json"




new_annotations = {
        "annotations": []
    }

# Lire le fichier JSON existant
with open(json_input_path, 'r') as file:
    data = json.load(file)

def get_best_match(label, categories):
    max_similarity = 0
    best_match = ""
    for category in categories:
        similarity = SequenceMatcher(None, label, category).ratio()
        if similarity > max_similarity:
            max_similarity = similarity
            best_match = category
    return best_match


def process_images_in_folder(input_folder, output_folder):
    # Assurez-vous que le dossier de sortie existe
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)
        #print("Euil")
    #j = 1
    # Boucle sur chaque fichier dans le dossier d'entrée
    for filename in os.listdir(input_folder):
        #print("Euil")
        #print(filename)
        # Vérifiez si le fichier est une image (ajoutez d'autres extensions si nécessaire)
        if filename.lower().endswith(('.png', '.jpg', '.jpeg', '.bmp', '.tiff')):
            # Chemin complet vers l'image
            #print(filename)
            img_path = os.path.join(input_folder, filename)
            
            model = load_model(CONFIG_PATH, WEIGHTS_PATH)
            image_source, image = load_image(img_path)
            boxes, logits, phrases = predict(
            model=model,
            image=image,
            caption=TEXT_PROMPT,
            box_threshold=BOX_TRESHOLD,
            text_threshold=TEXT_TRESHOLD
            )

            
            annotated_frame, box_coordinates, labels = annotate(image_source=image_source, boxes=boxes, logits=logits, phrases=phrases)


            #print("Les informations de l'image: " + img_path)
            #print(box_coordinates)
            #print(labels)
            #print("\n")


             # Trouver l'ID de l'image à partir de son nom de fichier
            image_id = None
            for image in data['images']:
                #print(image['file_name'])
                #print(filename)
                if image['file_name'] == filename:
                    image_id = image['id']
                    #print(image_id)
                    break
            
            if image_id is None:
                raise ValueError(f"Image ID not found for filename: {filename}")
        
            
            # Préparer les nouvelles annotations
            for i, (box, label) in enumerate(zip(box_coordinates, labels)):
                
                # Utiliser une expression régulière pour diviser label
                #print(label)
                match = re.match(r'^(.*)\s(\d+\.\d+)$', label)
                if match:
                    category_name, accuracy = match.groups()
                category_name = get_best_match(label, CATEGORIES)

                #print(category_name)
                #print(accuracy)
                
                category_id = None
                for category in data['categories']:
                    if category['supercategory'] == category_name:
                        category_id = category['id']
                        break
                
                if category_id is None:
                    raise ValueError(f"Category ID not found for category: {category_name}")
                #j = j+i
                new_annotation = {
                    "bbox": box.tolist(),  # Convertir le numpy array en liste
                    "image_id": image_id,
                    "category_id": category_id,
                    #"id": len(new_annotations['annotations']) +1 +i,  # Incremental ID
                    "accuracy": float(accuracy)
                }
                new_annotations['annotations'].append(new_annotation)

            # Nouveau nom pour l'image traitée
            base_name, ext = os.path.splitext(filename)
            new_filename = f"{base_name}_labelled{ext}"

            # Enregistrer l'image traitée

            new_img_path = os.path.join(output_folder, new_filename)
            cv2.imwrite(new_img_path, annotated_frame)
        #j = j+1
    for i, annotation in enumerate(new_annotations["annotations"], start=1):
        annotation["id"] = i
    # Sauvegarder les nouvelles annotations dans un nouveau fichier JSON
    with open(json_output_path, 'w') as file:
        json.dump(new_annotations, file, indent=2)         
           
# Exemple d'utilisation
process_images_in_folder(input_folder, output_folder)
