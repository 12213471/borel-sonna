#from groundingdino.util.inference import load_model, load_image, predict, annotate
#import cv2
import numpy as np
import cv2
import os
import json
import re
import supervision as sv
from difflib import SequenceMatcher
from groundingdino.util.inference import load_model, load_image, predict, annotate

# Load the GroundingDINO model with the specified configuration and weights
model = load_model("groundingdino/config/GroundingDINO_SwinB_cfg.py", "weights/groundingdino_swinb_cogcoor.pth")

# Path to the input image
IMAGE_PATH = "test_Dinh/P1397.14.5.jpg"

# Text prompt for object detection
TEXT_PROMPT = "the wings of planes"

# Threshold values for detection
BOX_THRESHOLD = 0.35
TEXT_THRESHOLD = 0.25

# Load the image
image_source, image = load_image(IMAGE_PATH)

# Perform prediction
boxes, logits, phrases = predict(
    model=model,
    image=image,
    caption=TEXT_PROMPT,
    box_threshold=BOX_THRESHOLD,
    text_threshold=TEXT_THRESHOLD
)

# Annotate the image with detected boxes
annotated_frame, box_coordinates, labels = annotate(image_source=image_source, boxes=boxes, logits=logits, phrases=phrases)

# Debug: Check the type and structure of annotated_frame
print(f"Type of annotated_frame: {type(annotated_frame)}")
print(f"Shape of annotated_frame: {annotated_frame.shape if isinstance(annotated_frame, np.ndarray) else 'N/A'}")

# Save the annotated image to a file
output_path = "test_Dinh/results/P1397.14.5_the wings of planes.jpg"
if isinstance(annotated_frame, np.ndarray):
    cv2.imwrite(output_path, annotated_frame)
else:
    print("annotated_frame is not a valid image array. Please check the annotate function output.")
