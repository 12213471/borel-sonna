from pycocotools.coco import COCO
from os.path import join
import time
import shutil
import json
#import cv2
import torch
from torch.utils.data import DataLoader, Dataset
import torchvision.transforms as transforms
import os
import torch.distributed as dist
import torch.multiprocessing as mp
from torch.nn.parallel import DistributedDataParallel as DDP
from transformers import AutoModelForCausalLM, AutoProcessor
from PIL import Image

LOCAL_RANK = int(os.environ['LOCAL_RANK'])
WORLD_SIZE = int(os.environ['WORLD_SIZE'])
WORLD_RANK = int(os.environ['RANK'])

class CustomCocoDataset(Dataset):
    def __init__(self, root, annFile, transform=None):
        """
        Args:
            root (string): Directory with all the images.
            annFile (string): Path to the json annotation file.
            transform (callable, optional): Optional transform to be applied
                on a sample.
        """
        self.root = root
        self.coco = COCO(annFile)
        # self.ids = list(self.coco.imgs.keys())
        self.ids = list(self.coco.imgs.keys())[63000:]
        
        self.transform = transform

    def __len__(self):
        return len(self.ids)

    def __getitem__(self, index):
        img_id = self.ids[index]
        #ann_ids = self.coco.getAnnIds(imgIds=img_id)
        #anns = self.coco.loadAnns(ann_ids)
        img = self.coco.loadImgs(img_id)[0]
        image_path = join(self.root, img['image_name'])
        image = Image.open(image_path)
        # image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        data = {'image': image, 'img_id': img_id}
        
        return data
    
def setup(rank, world_size):
    # os.environ['MASTER_ADDR'] = '127.0.0.1'
    # os.environ['MASTER_PORT'] = '29500'
    dist.init_process_group("nccl", rank=WORLD_RANK, world_size=WORLD_SIZE)
    #dist.init_process_group("nccl", rank=rank, world_size=world_size)

def get_dataloader(batch_size, rank, world_size, root, annFile):
    dataset = CustomCocoDataset(root, annFile)
    sampler = torch.utils.data.distributed.DistributedSampler(
        dataset, num_replicas=world_size, rank=rank)
    return DataLoader(dataset, batch_size=batch_size, sampler=sampler, num_workers=4, pin_memory=True, collate_fn= lambda x: x)

def run(image, task, model, processor, device):
    inputs = processor(text=task, images=image, return_tensors="pt")
    generated_ids = model.generate(
        input_ids=inputs["input_ids"].to(device),
        pixel_values=inputs["pixel_values"].to(device),
        max_new_tokens=1024,
        early_stopping=False,
        do_sample=False,
        num_beams=3,
    )
    generated_text = processor.batch_decode(generated_ids, skip_special_tokens=False)[0]
    parsed_answer = processor.post_process_generation(
        generated_text, 
        task=task, 
        image_size=(image.width, image.height)
    )
    return parsed_answer

def round_bbox_values(bboxes):
    """Arrondit les valeurs des bboxes au dixième près."""
    return [[round(value, 1) for value in bbox] for bbox in bboxes]



def update_image_info_no(jsondata, caption, informative, img_id):
    
    # Lecture du fichier JSON
    data_image = jsondata["images"][img_id]
    data_image["informative"] = informative
    data_image["caption"] = caption
    filename = data_image["image_name"]
    image_path = os.path.join(dataDir, filename)


    return jsondata,filename,image_path


#update_image_info_yes(annFile, image, caption, "yes", img_id)
def update_image_info_yes(jsondata, image, caption, informative, img_id, model, processor, device):
    
    # Lecture du fichier JSON
    # with open(annFile, 'r') as file:
    #     data = json.load(file)
    #breakpoint()
    data_image = jsondata["images"][img_id]
    #breakpoint()
    data_image["informative"] = informative
    data_image["caption"] = caption

    #json.dumps(data_image, indent=4)
    results = run(image, '<OD>', model, processor, device)
    results =   results.get('<OD>', '')
    bboxes =   results.get('bboxes', '')
    labels =   results.get('labels', '')
    print(labels)
    
    
    #breakpoint()
    if "annotations" not in jsondata:
        jsondata["annotations"] = []

    # Arrondir les valeurs des bboxes
    rounded_bboxes = round_bbox_values(bboxes)

    for bbox, label in zip(rounded_bboxes, labels):
        annotation = {
            "bbox": bbox,
            "label": label,
            "img_id" : img_id
        }
        jsondata["annotations"].append(annotation)

    filename = data_image["image_name"]
    image_path = os.path.join(dataDir, filename)

    # with open(output_annFile, 'w') as file:
    #     #print(json_filepath)
    #     #data = json.load(file)
    #     json.dump(data, file, indent=4)

    return jsondata, filename,image_path
           



def find_keyword_index(keywords, caption):
    for keyword in keywords:
        if keyword in caption:
            return keywords.index(keyword)
    return -1

def inference(rank, world_size, dataloader):
    setup(rank, world_size)
    #TODO create model
    model_id = 'microsoft/Florence-2-large'
    device = torch.device(f"cuda:{LOCAL_RANK}")
    model = AutoModelForCausalLM.from_pretrained(model_id, trust_remote_code=True).eval()
    model = model.to(device)
    processor = AutoProcessor.from_pretrained(model_id, trust_remote_code=True)

    with open(annFile, 'r') as file:
            jsondata = json.load(file)
    nombre = 0
    for data in dataloader:
        #print(data)
        image = data[0]['image']
        img_id = data[0]['img_id']
        #anns = data['anns']
        
        # Utiliser la fonction run avec le nouveau modèle
        #task = "<OD>"

        

        caption_dict = run(image, "<CAPTION>", model, processor, device) #TODO  pass the model
        caption = caption_dict.get('<CAPTION>', '')

        indice = find_keyword_index(keywords, caption)
        
        
        if keywords[indice] in caption:
            if any(additional_keyword in caption for additional_keyword in additional_keywords[indice]):
                jsondata, filename, image_path = update_image_info_yes(jsondata, image, caption, "yes", img_id, model, processor,device)
                shutil.move(image_path, os.path.join(output_folder_without_keyword, filename))
                #captions_without_keyword.append({'filename': filename, 'caption': caption, 'info': image_info})
            else:
                jsondata, filename,image_path = update_image_info_no(jsondata, caption, "no",img_id)
                shutil.move(image_path, os.path.join(output_folder_with_keyword, filename))
                        
        else:
            jsondata, filename,image_path = update_image_info_yes(jsondata, image, caption, "yes", img_id, model, processor, device)
            shutil.move(image_path, os.path.join(output_folder_without_keyword, filename))
        # Traiter et enregistrer les résultats
        # Ici, vous pouvez ajouter le code nécessaire pour gérer les résultats, comme les filtrer ou les enregistrer
        #print(f"Image ID: {img_id}, Result: {result}")
        print(nombre)
        nombre= nombre + 1
    with open(output_annFile, 'w') as file:
        #print(json_filepath)
        #data = json.load(file)
        json.dump(jsondata, file, indent=4)

if __name__ == "__main__":
    dataDir = "/home/bsonnafe/Florence/DATASET/DATASET_78"
    annFile = "/home/bsonnafe/Florence/JSON_reorganised_modify/78_reorganise_id.json"
    output_annFile = "/home/bsonnafe/Florence/JSON_reorganised_modify/78_reorganise_id"+str(LOCAL_RANK)+".json"
    # dataDir ="/home/bsonnafe/Florence/test_final"
    # annFile = "/home/bsonnafe/Florence/test_final/77_reorganise.json"
    # output_annFile = "/home/bsonnafe/Florence/test_final/json_test_final/77_reorganise"+str(LOCAL_RANK)+".json"
    area_threshold = 250  # Area threshold for removing small bounding boxes from SAM outputs
    iou_threshold = 0.55  # IOU threshold for keeping the SAM generated bbox after comparing with existing annotations

    results = []
    #input_folder = 'test_separate'
    # output_folder_with_keyword = '/home/bsonnafe/Florence/non_informative'
    # output_folder_without_keyword = '/home/bsonnafe/Florence/informative'
    output_folder_with_keyword = '/home/bsonnafe/Florence/DATASET/non_informative'
    output_folder_without_keyword = '/home/bsonnafe/Florence/DATASET/informative'
    #output_modified_json = 'JSON_reorganised_modify'
    keywords = ['forest','blurry','wooden surface', 'beige','desert','green grass', 'field']
    additional_keywords = [['forested', 'middle', 'in a forest','next to a forest'], [],[],[],['in the desert', 'middle of a desert'],[],['field with', 'field and', 'field surrounded by', 'surrounded', 'around','tractor', 'track', 'farm', 'next to', 'middle', 'near', 'down', 'up to','football','basket','ball','soccer field','in a field','dirt field', 'green field']]  # Liste des mots supplémentaires à rechercher
    

    start_time = time.time()

    if not os.path.exists(output_folder_with_keyword):
        os.makedirs(output_folder_with_keyword)
    if not os.path.exists(output_folder_without_keyword):
        os.makedirs(output_folder_without_keyword)

    dataloader = get_dataloader(batch_size=1, rank=WORLD_RANK, world_size=WORLD_SIZE, root=dataDir, annFile=annFile)
    
    inference(WORLD_RANK, WORLD_SIZE, dataloader)
    
    end_time = time.time()  # Ligne après la fonction

    execution_time = end_time - start_time
    print(f"Temps d'exécution de process_images: {execution_time} secondes")
        
