import json

def remove_category_and_annotations(input_file, output_file, category_id):
    # Charger les données du fichier JSON d'entrée
    with open(input_file, 'r') as f:
        data = json.load(f)

    # Retirer la catégorie spécifiée
    data['categories'] = [cat for cat in data['categories'] if cat['id'] != category_id]
    
    # Retirer les annotations associées à cette catégorie
    data['annotations'] = [ann for ann in data['annotations'] if ann['category_id'] != category_id]
    
    # Écrire les données modifiées dans le fichier JSON de sortie
    with open(output_file, 'w') as f:
        json.dump(data, f, indent=4)

# Exemple d'utilisation

category_id_to_remove = 10  # ID de la catégorie à supprimer
input_file = 'data/image_dataset/dota_dataset/coco/annotations/instances_test2017.json'  # Nom du fichier JSON d'entrée
output_file = 'deleted_class'+str(category_id_to_remove)+'.json'  # Nom du fichier JSON de sortie

# Appeler la fonction pour supprimer la catégorie et les annotations associées
remove_category_and_annotations(input_file, output_file, category_id_to_remove)

print(f"Les modifications ont été enregistrées dans le fichier {output_file}.")
