'''LA J AI EXECUTE CA EN LOCAL POUR SEPARER LES INFORMATIVE ET LES NON INFORMATIVE DE DATASET 77 DANS LE DOSSIER DATASET 77 78 95 CAR APRES AVOIR TRAITE EN LIGNE J AVAIS SUPPRIMER POUR QUESTION D ESPACE. DONC AYANT LE FICHIER JSON JE PEUX SEPARER'''





import json
import os
import shutil

# Variables pour les chemins des fichiers et répertoires
json_file_path = "C:/Users/SONNA F. Borel/Desktop/77_reorganise_id_annotated.json"
base_directory = "C:/Users/SONNA F. Borel/Desktop/DATASET 77 78 95"
informative_directory = "D:/DATASET ANNOTATED/DATASET_77/informative"
non_informative_directory = "D:/DATASET ANNOTATED/DATASET_77/non_informative"

# Créer les répertoires de destination s'ils n'existent pas
os.makedirs(informative_directory, exist_ok=True)
os.makedirs(non_informative_directory, exist_ok=True)

# Charger le fichier JSON
with open(json_file_path, 'r') as f:
    data = json.load(f)

# Parcourir les éléments du champ "images"
for image in data["images"]:
    # Obtenir le nom du fichier et la valeur du champ "informative"
    image_name = image["image_name"]
    informative_value = image["informative"]

    # Chemin complet du fichier source
    source_path = os.path.join(base_directory, image_name)

    # Vérifier si le fichier existe avant de le déplacer
    if os.path.exists(source_path):
        if informative_value == "yes":
            # Déplacer vers le répertoire "informative"
            destination_path = os.path.join(informative_directory, image_name)
        else:
            # Déplacer vers le répertoire "non_informative"
            destination_path = os.path.join(non_informative_directory, image_name)
        
        # Effectuer le déplacement
        shutil.move(source_path, destination_path)
        print(f"Déplacé: {source_path} -> {destination_path}")
    else:
        print(f"Fichier non trouvé: {source_path}")

print("Le traitement est terminé.")
