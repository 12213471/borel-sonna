import json

# Dictionnaire des nouveaux prompts pour chaque catégorie
new_prompts = {
    "plane": "Detect all types of aircraft, including planes and helicopters.",
    "ship": "Locate all types of watercraft, such as ships and boats.",
    "storage-tank": "Identify all storage structures, including large storage tanks.",
    "baseball-diamond": "Find the sports facilities, including baseball diamonds.",
    "tennis-court": "Find the sports facilities, including tennis courts.",
    "basketball-court": "Find the sports facilities, including basketball courts.",
    "ground-track-field": "Find the sports facilities, including ground track fields.",
    "harbor": "Locate the harbor areas and any ships or boats present.",
    "bridge": "Identify the bridges that span over roads, rivers, or other obstacles.",
    "small-vehicle": "Locate all vehicles, including small cars.",
    "large-vehicle": "Locate all vehicles, including large trucks and buses.",
    "roundabout": "Find roundabouts and other circular intersections used for traffic management.",
    "swimming-pool": "Detect any swimming pools, often found in residential backyards.",
    "helicopter": "Spot all types of aircraft, including helicopters.",
    "soccer-ball-field": "Find the sports facilities, including soccer fields.",
    "container-crane": "Identify all industrial equipment, including container cranes."
}

def replace_category_prompts(input_file, output_file):
    with open(input_file, 'r') as f:
        data = json.load(f)

    for category in data['categories']:
        category_name = category['name']
        if category_name in new_prompts:
            new_prompt = new_prompts[category_name]
            category['supercategory'] = new_prompt
            category['name'] = new_prompt

    with open(output_file, 'w') as f:
        json.dump(data, f, indent=2)

# Remplacer 'input.json' et 'output.json' par les noms de vos fichiers
input_file = 'data/image_dataset/dota_dataset/coco/annotations/instances_test2017.json'
output_file = 'new_instances_test2017.json'
replace_category_prompts(input_file, output_file)
