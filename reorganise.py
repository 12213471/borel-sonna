import json

# Fonction pour réorganiser la structure JSON
def reorganize_json(input_data):
    output_data = {
        "info": "IGN dataset coco format",
        "licenses": "None",
        "files": [],
        "images": []
    }
    
    for file in input_data:
        file_entry = {
            "id": file["id"],
            "location": file["location"],
            "date": file["date"],
            "resolution": file["resolution"],
            "filename": file["filename"],
            "x1": file["x1"],
            "y1": file["y1"],
            "x2": file["x2"],
            "y2": file["y2"],
            "CoordSys Earth": file["CoordSys Earth"]
        }
        output_data["files"].append(file_entry)
        
        for image in file["images"]:
            image_entry = {
                "image_id": image["image_id"],
                "file_id": file["id"],
                "image_name": image["image_name"],
                "x1": image["x1"],
                "y1": image["y1"],
                "x2": image["x2"],
                "y2": image["y2"]
            }
            output_data["images"].append(image_entry)
    
    return output_data

# Lecture du fichier JSON d'entrée
with open('/home/bsonnafe/Florence/JSON_reorganised_modify/75.json', 'r') as infile:
    input_data = json.load(infile)

# Réorganisation du JSON
output_data = reorganize_json(input_data)

# Écriture du JSON réorganisé dans un nouveau fichier
with open('/home/bsonnafe/Florence/JSON_reorganised_modify/75_reorganise.json', 'w') as outfile:
    json.dump(output_data, outfile, indent=4)

print("Le fichier JSON a été réorganisé et sauvegardé sous le nom 'output.json'.")
