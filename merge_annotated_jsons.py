import json

# Variables pour les chemins des fichiers
fichier1_path = '/home/bsonnafe/Florence/result78[:63000]/78_reorganise_id_annotated1.json'
fichier2_path = '/home/bsonnafe/Florence/result78[63000:]/78_reorganise_id_annotated2.json'
fichier_fusionne_path = '/home/bsonnafe/Florence/result78[63000:]/78_reorganise_id_annotated.json'

# Charger les deux fichiers JSON
with open(fichier1_path, 'r') as f1, open(fichier2_path, 'r') as f2:
    data1 = json.load(f1)
    data2 = json.load(f2)

# Initialiser le nouveau dictionnaire JSON avec les trois premiers champs du premier fichier
result = {
    "info": data1["info"],
    "licenses": data1["licenses"],
    "files": data1["files"]
}

# Fusionner les champs "images" en conservant tous les éléments qui possèdent l'attribut "informative"
result["images"] = [img for img in data1["images"] if "informative" in img]
result["images"].extend([img for img in data2["images"] if "informative" in img])

# Fusionner tous les éléments du champ "annotations"
result["annotations"] = data1["annotations"] + data2["annotations"]

# Sauvegarder le résultat dans un nouveau fichier JSON
with open(fichier_fusionne_path, 'w') as f_out:
    json.dump(result, f_out, indent=4)

print(f"Le fichier fusionné a été sauvegardé sous {fichier_fusionne_path}")
