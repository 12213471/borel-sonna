import json

# Liste des chemins d'accès aux fichiers JSON
file_paths = ['JSON_reorganised_modify/77_reorganise.json', 'JSON_reorganised_modify/78_reorganise.json', 'JSON_reorganised_modify/95_reorganise.json']

def load_json(file_path):
    """Charge un fichier JSON à partir du chemin d'accès spécifié."""
    with open(file_path, 'r') as file:
        return json.load(file)

def merge_json_files(file_paths):
    """Combine les champs 'files' et 'images' de plusieurs fichiers JSON."""
    merged_data = {
        "info": "",
        "licenses": "",
        "files": [],
        "images": []
    }

    for i, file_path in enumerate(file_paths):
        data = load_json(file_path)
        
        if i == 0:
            # Copier les champs 'info' et 'licenses' du premier fichier
            merged_data["info"] = data["info"]
            merged_data["licenses"] = data["licenses"]
        
        # Ajouter les entrées des champs 'files' et 'images' aux listes correspondantes
        merged_data["files"].extend(data["files"])
        merged_data["images"].extend(data["images"])

    return merged_data

def save_json(data, output_path):
    """Enregistre les données JSON dans un fichier spécifié."""
    with open(output_path, 'w') as file:
        json.dump(data, file, indent=4)

if __name__ == "__main__":
    # Fusionner les fichiers JSON
    merged_data = merge_json_files(file_paths)
    
    # Enregistrer le fichier fusionné
    save_json(merged_data, 'JSON_reorganised_modify/77_78_95_reorganise.json')
