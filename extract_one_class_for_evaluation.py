import json

def retain_category_and_annotations(input_file, output_file, category_id):
    # Charger les données du fichier JSON d'entrée
    with open(input_file, 'r') as f:
        data = json.load(f)

    # Conserver uniquement la catégorie spécifiée
    data['categories'] = [cat for cat in data['categories'] if cat['id'] == category_id]
    
    # Conserver uniquement les annotations associées à cette catégorie
    data['annotations'] = [ann for ann in data['annotations'] if ann['category_id'] == category_id]
    
    # Écrire les données modifiées dans le fichier JSON de sortie
    with open(output_file, 'w') as f:
        json.dump(data, f, indent=4)

# Exemple d'utilisation

category_id_to_retain = 16  # ID de la catégorie à conserver
input_file = 'new_instances_test2017.json'  # Nom du fichier JSON d'entrée
output_file = 'extracted_class'+str(category_id_to_retain)+'.json'  # Nom du fichier JSON de sortie

# Appeler la fonction pour conserver uniquement la catégorie et les annotations associées
retain_category_and_annotations(input_file, output_file, category_id_to_retain)

print(f"Les modifications ont été enregistrées dans le fichier {output_file}.")



