import json

def modify_json(input_file, output_file):
    # Lire le fichier JSON d'entrée
    with open(input_file, 'r') as infile:
        data = json.load(infile)

    # Supprimer les champs "info" et "licenses" à la racine du JSON
    if "info" in data:
        del data["info"]
    if "licenses" in data:
        del data["licenses"]
   

    # Supprimer les champs inutiles dans "images"
    for image in data.get("images", []):
        keys_to_remove = ["height", "width", "date_captured", "flickr_url", "license", "coco_url"]
        for key in keys_to_remove:
            if key in image:
                del image[key]

    # Supprimer les champs inutiles dans "annotations"
    for annotation in data.get("annotations", []):
        keys_to_remove = ["area", "segmentation", "iscrowd"]
        for key in keys_to_remove:
            if key in annotation:
                del annotation[key]

    # Supprimer les champs inutiles dans "categories"
    for category in data.get("categories", []):
        keys_to_remove = ["name"]
        for key in keys_to_remove:
            if key in category:
                del category[key]

    # Sauvegarder les données modifiées dans un nouveau fichier JSON
    with open(output_file, 'w') as outfile:
        json.dump(data, outfile, indent=2)

    print(f"Les données ont été modifiées et sauvegardées dans '{output_file}'.")

# Nom des fichiers d'entrée et de sortie
input_file = 'data/image_dataset/dota_dataset/coco/annotations/instances_test2017.json'
output_file = 'data/image_dataset/dota_dataset/coco/annotations/instances_test2017_modified_data.json'

# Appel de la fonction pour modifier le JSON
modify_json(input_file, output_file)
