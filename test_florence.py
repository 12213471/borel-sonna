from transformers import AutoProcessor, AutoModelForCausalLM  
from PIL import Image
import requests
import json
import copy


model_id = 'microsoft/Florence-2-large'
model = AutoModelForCausalLM.from_pretrained(model_id, trust_remote_code=True).eval().cuda()
processor = AutoProcessor.from_pretrained(model_id, trust_remote_code=True)



def run_example(task_prompt, text_input=None):
    if text_input is None:
        prompt = task_prompt
    else:
        prompt = task_prompt + text_input
    inputs = processor(text=prompt, images=image, return_tensors="pt")
    generated_ids = model.generate(
      input_ids=inputs["input_ids"].cuda(),
      pixel_values=inputs["pixel_values"].cuda(),
      max_new_tokens=1024,
      early_stopping=False,
      do_sample=False,
      num_beams=3,
    )
    generated_text = processor.batch_decode(generated_ids, skip_special_tokens=False)[0]
    parsed_answer = processor.post_process_generation(
        generated_text, 
        task=task_prompt, 
        image_size=(image.width, image.height)
    )

    return parsed_answer

def round_bbox_values(bboxes):
    """Arrondit les valeurs des bboxes au dixième près."""
    return [[round(value, 1) for value in bbox] for bbox in bboxes]

# Fonction principale
def add_annotations_to_json(file_path, bboxes, labels):
    # Lire le fichier JSON existant
    with open(file_path, 'r') as file:
        data = json.load(file)
    
    # Vérifier si le champ annotations existe, sinon le créer
    if 'annotations' not in data:
        data['annotations'] = []

    # Arrondir les valeurs des bboxes
    rounded_bboxes = round_bbox_values(bboxes)

    # Ajouter les nouvelles annotations
    for bbox, label in zip(rounded_bboxes, labels):
        annotation = {
            'bbox': bbox,
            'label': label
        }
        data['annotations'].append(annotation)

    # Écrire les modifications dans le fichier JSON
    with open(file_path, 'w') as file:
        json.dump(data, file, indent=4)


url = "https://huggingface.co/datasets/huggingface/documentation-images/resolve/main/transformers/tasks/car.jpg?download=true"
image = Image.open(requests.get(url, stream=True).raw)



task_prompt = '<CAPTION>'
print(run_example(task_prompt))

task_prompt = '<OD>'
results = run_example(task_prompt)
results =   results.get('<OD>', '')
bboxes =   results.get('bboxes', '')
labels =   results.get('labels', '')
print(bboxes)
print(labels)



# Utilisation de la fonction
file_path = 'votre_fichier.json'
add_annotations_to_json(file_path, bboxes, labels)