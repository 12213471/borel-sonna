import json

# Charger le fichier JSON initial
with open('/home/bsonnafe/Florence/JSON_reorganised_modify/78_reorganise.json', 'r') as file:
    data = json.load(file)

# Ajouter un champ `id` incrémental à chaque élément de "images"
for index, image in enumerate(data['images']):
    image['id'] = index

# Sauvegarder le résultat dans un nouveau fichier JSON
with open('/home/bsonnafe/Florence/JSON_reorganised_modify/78_reorganise_id.json', 'w') as file:
    json.dump(data, file, indent=4)

print("Transformation terminée. Le fichier 'output.json' a été créé.")
